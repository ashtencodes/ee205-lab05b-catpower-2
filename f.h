/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file ev.h
/// @version 1.0
///
/// @author Ashten Akemoto <aakemoto@hawaii.edu>
/// @date 02-14-2022
/////////////////////////////////////////////////////////////////////////////
//
#pragma once

const double FOE_IN_A_JOULE  = 1e44;

const char FOE = 'f';

extern double fromFoeToJoule( double foe ) ;
extern double fromJouleToFoe( double joule ) ;