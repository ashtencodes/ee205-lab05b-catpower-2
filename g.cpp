//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file ev.h
/// @version 1.0
///
/// @author Ashten Akemoto <aakemoto@hawaii.edu>
/// @date 02-14-2022
/////////////////////////////////////////////////////////////////////////////

#include "g.h"

double fromGasolineToJoule( double gasoline ) {
    return gasoline * GASOLINE_IN_A_JOULE;
}

double fromJouleToGasoline( double joule ) {
    return joule * GASOLINE_IN_A_JOULE;
}