###############################################################################
### University of Hawaii, College of Engineering
### @brief Lab 05b - catPower 2 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0 - Initial version
###
### Build and test an energy unit conversion program
###
### @author Ashten Akemoto <aakemoto@hawaii.edu>
### @date 02-14-21
###
### @see https://www.gnu.org/software/make/manual/make.html
###############################################################################
CC = g++
CFLAGS = -g -Wall -Wextra
TARGET = catPower

all: $(TARGET)

ev.o: ev.cpp ev.h
	$(CC) $(CFLAGS) -c ev.cpp

c.o: c.cpp c.h
	$(CC) $(CFLAGS) -c c.cpp

f.o: f.cpp f.h
	$(CC) $(CFLAGS) -c f.cpp

g.o: g.cpp g.h
	$(CC) $(CFLAGS) -c g.cpp

m.o: m.cpp m.h
	$(CC) $(CFLAGS) -c m.cpp

catPower.o: catPower.cpp ev.h c.h f.h g.h m.h
	$(CC) $(CFLAGS) -c catPower.cpp

catPower: catPower.o ev.o f.o g.o m.o c.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o ev.o f.o g.o m.o c.o

clean:
	rm -f $(TARGET) *.o

test: catPower
	@./catPower 3.14 j j | grep -q "3.14 j is 3.14 j"
	@./catPower 3.14 j e | grep -q "3.14 j is 1.95983E+19 e"
	@./catPower 3.14 j m | grep -q "3.14 j is 7.50478E-16 m"
	@./catPower 3.14 j g | grep -q "3.14 j is 2.58862E-08 g"
	@./catPower 3.14 j f | grep -q "3.14 j is 3.14E-44 f"
	@./catPower 3.14 j c | grep -q "3.14 j is 0 c"
	@./catPower 3.14 j x |& grep -q "Unknown toUnit x"
	@./catPower 1 m j |& grep -q "1 m is 4.184E+15 j"
	@./catPower 1 g e |& grep -q "1 g is 5.14552E+10 e"
	@./catPower 1 f m |& grep -q "1 f is 2.39006E+28 m"
	@./catPower 1 c g |& grep -q "1 c is 0 g"
	@./catPower 1 x f |& grep -q "Unknown fromUnit x"
	@echo "All tests pass"