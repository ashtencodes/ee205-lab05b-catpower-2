/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file ev.h
/// @version 1.0
///
/// @author Ashten Akemoto <aakemoto@hawaii.edu>
/// @date 02-14-2022
/////////////////////////////////////////////////////////////////////////////

#include "ev.h"

double fromElectronVoltsToJoule( double electronVolts ) {
    return electronVolts / ELECTRON_VOLTS_IN_A_JOULE ;
}

double fromJouleToElectronVolts( double joule ) {
    return joule * ELECTRON_VOLTS_IN_A_JOULE;
}